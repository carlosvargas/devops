
#!/bin/bash
#
# Name: convertip2hex.sh
# Author: Carlos Vargas (ContainerKing.Ninja/CarlosVargas.com)
#
# Example:  ./convertip2hex.sh 192.168.1.1
IP_ADDR=$1
# If you want to see the value passed to the script
# echo $1
# echo $IP_ADDR
printf '%02X' ${IP_ADDR//./};echo

