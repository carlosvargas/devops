<H1> convertip2hex 

This script will help you convert IP address to hex. This is used in DHCP deployments automation

<h2> Download Script </h2>
To download follow these step
curl https://gitlab.com/carlosvargas/devops/raw/master/convertip2hex/convertip2hex.sh convertip2hex.sh && chmod +x convertip2hex.sh 

<h2> Execute Script </h2>
 ./convertip2hex.sh 192.168.1.1

 12539AB

