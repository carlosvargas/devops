#!/bin/bash
#
# Name:centostemplate.sh
# Author: Carlos Vargas
#
# Stop rsyslog and audit daemon
service rsyslog stop
service auditd stop

# Remove Old kernel packages
/bin/package-cleanup –oldkernels –count=1

# Clean yum packages
/usr/bin/yum clean all

# Clean Logs files
logrotate -f /etc/logrotate.conf
rm -f /var/log/*-???????? /var/log/*.gz
rm -f /var/log/dmesg.old
rm -rf /var/log/anaconda
cat /dev/null > /var/log/audit/audit.log
cat /dev/null > /var/log/wtmp
cat /dev/null > /var/log/lastlog
cat /dev/null > /var/log/grubby

# Remove Old UUID and ifcfg scripts
rm -f /etc/udev/rules.d/70*
netconfig=$(ls /etc/sysconfig/network-scripts/ifcfg-eno*)
#sed –i”.bak” ‘/UUID/d’ $netconfig
sudo sed -i '/UUID/d' $netconfig

# Remove SSH Keys
sudo rm -f /etc/ssh/*key*

# Remove bash history
rm -f ~root/.bash_history
unset HISTFILE

# Remove root user bash history Finally we are going remove root users SSH history and then shutdown for template creation
sudo rm -rf ~root/.ssh/
sudo history –c

sudo sys-unconfig