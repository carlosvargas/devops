#!/bin/bash
#
#

echo "Configure Hostname Script"
curl https://gitlab.com/carlosvargas/devops/raw/master/CentOS/hostname>/tmp/hostname && chmod +x /tmp/hostname && sudo cp /tmp/hostname /etc/init.d/
ln -s /etc/init.d/hostname /etc/rc3.d/S11hostname

#
#
# Configure EPEL Repo
curl https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm> /tmp/epel-release-latest-7.noarch.rpm
rpm -iUvh /tmp/epel-release-latest-7.noarch.rpm
yum update -y
yum install python python-pip wget tar xz unzip curl ipset open-vm-tools expect ansible -y
pip install virtualenv

#
#
# Configure SSH Keys
mkdir -p ~/.ssh
chown 600 ~/.ssh
ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -P ""
cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys

#
#
#
# Disable Firewall
echo "Disable Firewall"
systemctl stop firewalld && systemctl disable firewalld

#
#
# Install Docker
echo "Upgrade All Packages and configure OverlayFS"
yum upgrade --assumeyes --tolerant
yum update --assumeyes
tee /etc/modules-load.d/overlay.conf <<-'EOF'
overlay
EOF

#
#
#
# Install Docker Repo
echo "Configure Docker Repo"
tee /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/$releasever/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF

#
#
# Overlayfs config for docker
echo "Add overlay settings for Docker"
echo "{"storage-driver":"overlay"}" > /etc/docker/daemon.json

#
#
# Install Docker Engine
yum install --assumeyes --tolerant docker-engine
systemctl start docker
systemctl enable docker

# Configure SELinux
sed -i s/SELINUX=enforcing/SELINUX=permissive/g /etc/selinux/config && groupadd nogroup && reboot
